public class GameManager {
	private Deck drawPile;
	private Card centerCard;
	private Card playerCard;

public GameManager () {
	this.drawPile = new Deck();
	drawPile.shuffle();
	this.centerCard = drawPile.drawTopCard();
	this.playerCard = drawPile.drawTopCard();
}
public String toString () {
		return "Center Card:" + this.centerCard + "\n" + "Player Card:" + this.playerCard;
	} 
public void dealCards () {
Card card1 = drawPile.drawTopCard();
centerCard = card1;
drawPile.shuffle();
Card card2 = drawPile.drawTopCard();
drawPile.shuffle();
playerCard = card2;
				
}
public int getNumberOfCards() {
	int NumberOfCards = drawPile.length();
	return NumberOfCards;
}
public int CalculatePoints() {
	if (centerCard.getValue().equals(playerCard.getValue())){
		return 4;
	} else if (centerCard.getSuit().equals(playerCard.getSuit())) {
		return 2;
	} else {
		return -1;
	}
}
}
	
	
	
