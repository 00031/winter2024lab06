public class Card {
	private String suit;
	private String value;
	// get method	
	public String getSuit () {
		return this.suit;
	} 
	public String getValue (){
		return this.value;
	}

	// constructor
	public Card (String suit, String value) {
		this.suit = suit;
		this.value = value;
	  } 
	  // toString method
	public String toString () {
		return this.value + " of " + this.suit;
	}
}